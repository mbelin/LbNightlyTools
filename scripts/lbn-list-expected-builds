#!/usr/bin/env python
###############################################################################
# (c) Copyright 2013 CERN                                                     #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Simple script to extract the list of expected builds.

The output format is JSON, and the expected builds are recorded as a list of
tuples, each containing:

 - binary tarball name
 - slot name
 - slot id
 - project name
 - platform
 - timestamp of the request
 - OS label

'''
__author__ = 'Marco Clemencic <marco.clemencic@cern.ch>'

import os
import sys
import time
import json
import re

from datetime import datetime, date

from LbNightlyTools.Scripts.Build import genPackageName

from LbNightlyTools.Scripts.Common import BaseScript

class Script(BaseScript):
    '''
    Script to print the list of binaries expected from the build.
    '''
    def defineOpts(self):
        '''Define options.'''
        from LbNightlyTools.Scripts.Common import addBasicOptions

        self.parser.add_option('--platforms', action='store',
                               help='whitespace-separated list of platforms to '
                                    'consider [default: those specified in the '
                                    'configuration]')

        self.parser.add_option('-o', '--output', action='store',
                               help='output file name '
                                    '[default: standard output]')

        addBasicOptions(self.parser)

    def main(self):
        '''
        Main function of the script.
        '''
        self._setup(make_dirs=False)
        opts = self.options

        now = time.time()

        os_label = os.environ.get('os_label', '')
        # remove (optional) suffix '-build' (or '-release') from os_label
        os_label = re.sub(r'-(build|release)$', '', os_label)

        if not opts.platforms:
            opts.platforms = self.slot.platforms
        else:
            opts.platforms = opts.platforms.split()

        expected_builds = [ (genPackageName(proj, platform,
                                            build_id=opts.build_id,
                                            artifacts_dir=opts.artifacts_dir),
                             self.slot.name,
                             self.slot.build_id,
                             proj.name,
                             platform,
                             now,
                             os_label)
                            for proj in self.slot.activeProjects
                            for platform in opts.platforms
                            if not proj.no_test ]

        self.log.debug('expecting %d builds', len(expected_builds))

        if opts.output:
            import codecs
            json.dump(expected_builds, codecs.open(opts.output, 'w', 'utf-8'),
                      indent=2)
        else:
            print json.dumps(expected_builds, indent=2)


# __main__
sys.exit(Script().run())
